#!/usr/bin/env bash

set -eo pipefail

NAMESPACE=${1}
ARCH=${2:-"x86_64"}

terraform init --upgrade

terraform apply -var "namespace=${NAMESPACE}" -var "arch=${ARCH}"

INSTANCE_IP=$(terraform output -raw instance_ip)

echo "All set up run the following command"
echo "ssh -i ~/.ssh/aws.pem ec2-user@${INSTANCE_IP}"
