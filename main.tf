terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region = "eu-west-1"
}


##################################
# Networking
##################################

resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name  = "${var.namespace}-vpc"
    Group = "runner"
    Slack = var.namespace
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name  = "${var.namespace}-gw"
    Group = "runner"
    Slack = var.namespace
  }
}

resource "aws_route_table" "rt" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  route {
    ipv6_cidr_block = "::/0"
    gateway_id      = aws_internet_gateway.gw.id
  }

  tags = {
    Name  = "${var.namespace}-rt"
    Group = "runner"
    Slack = var.namespace
  }
}

resource "aws_subnet" "subnet_1" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "eu-west-1c"

  tags = {
    Name  = "${var.namespace}_subnet_1"
    Group = "runner"
    Slack = var.namespace
  }
}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.subnet_1.id
  route_table_id = aws_route_table.rt.id
}

resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  description = "Allow ssh inbound traffic"
  vpc_id      = aws_vpc.main.id

  ingress {
    description = "ssh from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "${var.namespace}_allow_ssh"
    Group = "runner"
    Slack = var.namespace
  }
}

resource "aws_network_interface" "ssh_nic" {
  subnet_id       = aws_subnet.subnet_1.id
  private_ips     = ["10.0.1.50"]
  security_groups = [aws_security_group.allow_ssh.id]
}

resource "aws_eip" "ssh" {
  vpc                       = true
  network_interface         = aws_network_interface.ssh_nic.id
  associate_with_private_ip = "10.0.1.50"
  depends_on                = [aws_internet_gateway.gw]
}

##################################
# ec2 instance
##################################

data "aws_ami" "amzn2" {
  most_recent = true

  filter {
    name = "name"
    // amzn2-ami-hvm-2.0.20210326.0-arm64-gp2
    // amzn2-ami-hvm-2.0.20210326.0-x86_64-gp2
    values = ["amzn2-ami-*-${var.arch}-gp2"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "architecture"
    values = [var.arch]
  }

  owners = ["amazon"]
}

resource "aws_instance" "runner" {
  ami               = data.aws_ami.amzn2.id
  instance_type     = var.instance_type[var.arch]
  availability_zone = "eu-west-1c"
  key_name          = var.namespace

  network_interface {
    network_interface_id = aws_network_interface.ssh_nic.id
    device_index         = 0
  }

  tags = {
    Name  = "${var.namespace}-runner"
    Group = "runner"
    Slack = var.namespace
  }
}
