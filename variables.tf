variable "namespace" {
  type        = string
  description = "The namespace to prefix all resources. This is used to get the keypair and to tag all resources as well. Ideally should be your slack/gitlab username."
}

variable "arch" {
  type        = string
  description = "Architecture for the ec2 instance"
  default     = "x86_64"
}

variable "instance_type" {
  type        = map(any)
  description = "The instance type associated with the architecture"
  default = {
    "x86_64" = "t3.micro"
    "arm64"  = "a1.medium"
  }
}
