#!/usr/bin/env bash

set -eo pipefail

NAMESPACE=${1}

terraform destroy -var "namespace=${NAMESPACE}"
