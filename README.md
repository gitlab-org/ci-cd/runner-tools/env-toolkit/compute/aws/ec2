# ec2

Create and delete an EC2 instance either `x86_64` or `arm64` inside of its own VPC with SSH available.

## Prerequisites

- [Terraform](https://www.terraform.io/)
- [AWS Authentication](https://registry.terraform.io/providers/hashicorp/aws/latest/docs#authentication),
  if you use [aws-cli](https://aws.amazon.com/cli/) you should be set up.
- [SSH EC2 Key Pair](https://docs.aws.amazon.com/ground-station/latest/ug/create-ec2-ssh-key-pair.html), ideally with your GitLab/Slack username as a name.
  
## Up

- `$NAMESPACE`: The namespace to used to prefix all resources and tag them
properly. This **needs** to be the same name as your Key Pair and ideally to
your GitLab/Slack username.
- `$ARCH`: The architecture for the ec2. Either `x86_64` (default) or `arm64`.

```shell
./up.sh $NAMESPACE $ARCH

# Set up x86_64 machine, with Key Pair `sazzopardi`
./up.sh sazzopardi

# Set up arm64 machine, with Key Pair `sazzopardi`
./up.sh sazzopardi arm64

# Assuming you have the Key Pair available in ~/.ssh/aws.pem, INSTANCE_IP printed from previous command.
ssh -i ~/.ssh/aws.pem ec2-user@INSTANCE_IP
```

## Down

- `$NAMESPACE`: The namespace used in the `up.sh` command.

```shell
./down.sh $NAMESPACE

# Delete previous instance
./down.sh sazzopardi
```
